///////////////////
//Marco Biaggio
//CSE 002
//Homework 2
//June 25
//////////////////
public class Arithmetic {

	public static void main(String[] args) {
	  	//Number of pairs of pants
	    int numPants = 3;
	    //Cost per pair of pants
	    double pantsPrice = 34.98;
	    //Number of sweatshirts
	    int numShirts = 2;
	    //Cost per sweatshirt
	    double shirtPrice = 24.99;
	    //Number of belts
	    int numBelts = 1;
	    //cost per belt
	    double beltCost = 33.99;
	    double paSalesTax = 0.06;
	     
	    double totalCostOfPants = numPants * pantsPrice;
	    double totalCostOfShirts = numShirts * shirtPrice;
	    double totalCostOfBelts = numBelts * beltCost;
	     
	    double totalCostOfEverything = totalCostOfPants + totalCostOfShirts + totalCostOfBelts;
	    double totalCostWithTax = totalCostOfEverything + totalCostOfEverything*paSalesTax;
	     
	    System.out.println("First item being bought is pants.");
	    System.out.println("The number of pants bought: "+numPants);
	    System.out.println("The cost for one pair of pants: $"+pantsPrice);
	    System.out.println("Total cost of pants: $"+totalCostOfPants);
	    System.out.println("Sales tax: %"+paSalesTax*100);
	    System.out.println();
	    System.out.println("Second item being bought is sweatshirts.");
	    System.out.println("The number of sweatshirts bought: "+numShirts);
	    System.out.println("The cost for one sweatshirt: $"+shirtPrice);
	    System.out.println("Total cost of sweatshirts: $"+totalCostOfShirts);
	    System.out.println("Sales tax: %"+paSalesTax*100);
	    System.out.println();
	    System.out.println("Third item being bought is belts.");
	    System.out.println("The number of belts bought: "+numBelts);
	    System.out.println("The cost for one belt: $"+beltCost);
	    System.out.println("Total cost of belts: $"+totalCostOfBelts);
	    System.out.println("Sales tax: %"+paSalesTax*100);
	    System.out.println();
	    System.out.println("Total cost of the purchase not including tax: $" +totalCostOfEverything);
	    System.out.println("Total sales tax: %"+paSalesTax*100);
	    System.out.println("Total cost of the purchase including tax: $"+totalCostWithTax);

	}

}
