////////////////////
//Marco Biaggio
//Homework 03
//CSE 002
//September 14, 2018
//Prof. Carr
////////////////////

//importing the scanner so that we can take user input
import java.util.Scanner;
public class Pyramid {
  //main method required for every java program.
  public static void main(String[] args) {
    //declaring and constructing the scanner so i can take user input
    Scanner scnr = new Scanner(System.in);
    //declaring useful variables
    double pyramidSquareSide;
    double pyramidHeight;
    double pyramidVolume;
    
    //asking the user for the length of the square side
    System.out.print("The square side of the pyramid is (input length): ");
    pyramidSquareSide = scnr.nextDouble();
    
    //asking the user for the height of the pyramid
    System.out.print("The height of the pyramid is (input height): ");
    pyramidHeight = scnr.nextDouble();
    
    //calculating the volume of the pyramid
    pyramidVolume = (pyramidSquareSide * pyramidSquareSide * pyramidHeight) / 3;
    
    //giving the user what they crave
    System.out.println("The volume inside the pyramid is: " + pyramidVolume);
    
  }//end of the main method
}//end of the class