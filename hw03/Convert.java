////////////////////
//Marco Biaggio
//Homework 03
//CSE 002
//September 14, 2018
//Prof. Carr
////////////////////

//importing the scanner so that we can take user input
import java.util.Scanner;
public class Convert {
  //main method required for every java program.
  public static void main(String[] args) {
    //declaring and constructing a Scanner to take user input.
    Scanner scnr = new Scanner(System.in);
    
    //declaring userful variables
    double numAcresOfLand;
    double inchesRain;
    double convertedRainCubicMiles;
    double gallonsRain;
    //declaring useful constants
    final double GALLONS_PER_ACRE_PER_INCH_RAIN = 27154;
    final double CUBIC_MILES_PER_ONE_GALLON = 9.0816859724e-13;
    
    //asking the user for number of acres
    System.out.print("Enter the affected area in acres: ");
    numAcresOfLand = scnr.nextDouble();
    
    //asking the suer for inches of rainfall
    System.out.print("Enter the rainfall in inches in the affected area: ");
    inchesRain = scnr.nextDouble();
    
    //converting into cubic miles.
    gallonsRain = numAcresOfLand * inchesRain * GALLONS_PER_ACRE_PER_INCH_RAIN;
    convertedRainCubicMiles = gallonsRain * CUBIC_MILES_PER_ONE_GALLON;
    
    //giving the user what they crave.
    System.out.println(convertedRainCubicMiles + " cubic miles");
    
    /*
    the homework assignmet on google docs gives a different result than what i get.
    but i checked it with an online calculator,
    and the assignment input and output doesnt match what i put in online.
    Therefore I believe there might be an error in what is given on the google doc.
    */
    
    
  }//end of the main method
}//end of the class