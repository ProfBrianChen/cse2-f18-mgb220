//////////////////////////
//Marco Biaggio
//
//Lab 02
//
//May 29, 2018
//////////////////////////


public class Cyclometer {
	//main method required for every java program
	public static void main(String[] args) {
		
    //initializing variables for our input data.
    int secsTrip1 = 480; //Seconds it took to complete trip one.
    int secsTrip2 = 3220; // seconds it took to complete trip two.
    int countsTrip1 = 1561; //how many times the wheel rotated during trip one.
    int countsTrip2 = 9037; //how many times the wheel rotated during trip two.
    
    //initializing variables for intermidiate steps and output data.
    double wheelDiameter = 27.0; // the diamater of the bicycle wheel
    double PI = 3.14159; // the constant PI.
    int feetPerMile = 5280; // how many feet are in a mile.
    int inchesPerFoot = 12; //how many inches are in a foot.
    int secondsPerMinute = 60; //how many seconds are in a minute.
    double distanceTrip1, distanceTrip2, totalDistance; //The distance for each trip separatley, and in total.
    
    System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute) + " minutes and had " + countsTrip1 + " counts.");
    
    System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute) + " minutes and had " + countsTrip2 + " counts.");
    
    distanceTrip1 = countsTrip1 * wheelDiameter * PI; //distance travled in trip one 
    //(for each count, a rotation of the wheel travelsthe diameter in inches times PI)
    //above gives distance in inches, and we want miles.
    distanceTrip1 /= inchesPerFoot * feetPerMile; //this gives the distance in miles
    
    //we do the same thing for trip two.
    distanceTrip2 = (countsTrip2 * wheelDiameter * PI) / (inchesPerFoot * feetPerMile); 
    
    totalDistance = distanceTrip1 + distanceTrip2; //finally we calculate the total distance travled.
    
    //Print out the output data.
    System.out.println("Trip 1 was " + distanceTrip1 + " miles.");
    System.out.println("Trip 2 was " + distanceTrip2 + " miles.");
    System.out.println("The total distance was " + totalDistance + " miles.");


	}//end of main method

}//end of class